package com.examen.trupper.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.trupper.model.Compra;

public interface ICompraRepo extends JpaRepository<Compra, Integer> {

}
