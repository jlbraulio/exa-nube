package com.examen.trupper.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.trupper.model.Producto;

public interface IProductoRepo extends JpaRepository<Producto, Integer>{

}
