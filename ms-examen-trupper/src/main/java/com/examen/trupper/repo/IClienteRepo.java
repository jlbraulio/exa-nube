package com.examen.trupper.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.trupper.model.Cliente;

public interface IClienteRepo extends JpaRepository<Cliente, Integer>{

}
