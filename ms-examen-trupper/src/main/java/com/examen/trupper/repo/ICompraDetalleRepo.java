package com.examen.trupper.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.trupper.model.CompraDetalle;

public interface ICompraDetalleRepo extends JpaRepository<CompraDetalle, Integer> {

}
