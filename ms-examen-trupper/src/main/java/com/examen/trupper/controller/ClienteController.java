package com.examen.trupper.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.trupper.model.Cliente;
import com.examen.trupper.service.IClienteService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/clientes")
public class ClienteController {

	private final Logger logger = LoggerFactory.getLogger(ClienteController.class);
	
	private final IClienteService service;
	
	@PostMapping
	public ResponseEntity<Cliente> registrar(@RequestBody Cliente cliente) {
		Cliente clienteResponse = null;
		try {

			clienteResponse = service.registrar(cliente);
		} catch (Exception e) {
			logger.error("Error en registro cliente {}", e.getMessage());
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(clienteResponse, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> obtenerPorId(@PathVariable("id") Integer id) {
		Cliente cliente = null;
		try {

			cliente = service.obtenerPorId(id);
			
		} catch (Exception e) {
			logger.error("Error en obtener cliente por id {}", e.getMessage());
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(cliente, HttpStatus.OK);
		
	}
}
