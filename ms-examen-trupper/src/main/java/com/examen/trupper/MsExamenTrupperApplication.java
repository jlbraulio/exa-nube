package com.examen.trupper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsExamenTrupperApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsExamenTrupperApplication.class, args);
	}

}
