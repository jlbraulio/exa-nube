package com.examen.trupper.service.impl;

import org.springframework.stereotype.Service;

import com.examen.trupper.model.Producto;
import com.examen.trupper.repo.IProductoRepo;
import com.examen.trupper.service.IProductoService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ProductoServiceImpl implements IProductoService {

	private final IProductoRepo repo;
		
	@Override
	public Producto registrar(Producto obj) {		 
		return repo.save(obj);
	}

}
