package com.examen.trupper.service;

import com.examen.trupper.model.Producto;

public interface IProductoService {

	Producto registrar(Producto pro);
}
