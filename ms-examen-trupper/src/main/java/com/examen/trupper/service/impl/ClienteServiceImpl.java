package com.examen.trupper.service.impl;

import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.examen.trupper.model.Cliente;
import com.examen.trupper.repo.IClienteRepo;
import com.examen.trupper.service.IClienteService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ClienteServiceImpl implements IClienteService {

	private final IClienteRepo repo;
	
	@Override
	public Cliente registrar(Cliente cliente) {
		return  repo.save(cliente);		 
	}

	@Override
	public Cliente obtenerPorId(Integer idCliente) {
		Optional<Cliente> response = repo.findById(idCliente);
		return Objects.nonNull(response) ? response.get() : new Cliente();
	}

}
