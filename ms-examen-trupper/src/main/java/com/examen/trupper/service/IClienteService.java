package com.examen.trupper.service;

import com.examen.trupper.model.Cliente;

public interface IClienteService {

	Cliente registrar(Cliente cliente);
	
	Cliente obtenerPorId(Integer idCliente);
}
