package com.examen.trupper.service;

import com.examen.trupper.model.Compra;

public interface ICompraService {

	Compra registrar(Compra obj);
}
