package com.examen.trupper.service.impl;

import org.springframework.stereotype.Service;

import com.examen.trupper.model.Compra;
import com.examen.trupper.repo.ICompraRepo;
import com.examen.trupper.service.ICompraService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CompraServiceImpl implements ICompraService{

	private final ICompraRepo repo;
	
	@Override
	public Compra registrar(Compra obj) {		
		return repo.save(obj);
	}

}
