package com.examen.trupper.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="compra_detalle")
public class CompraDetalle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCompraDetalle;
	
	@Column(name="codigo_producto")
	private String codigoProducto;

	@Column(name="cantidad")
	private Integer cantidad;
	
	@ManyToOne
	@JoinColumn(name="id_compra", nullable = false, foreignKey = @ForeignKey(name="FK_lista_compra"))
	private Compra compra;
		
	@ManyToOne
	@JoinColumn(name="id_producto", nullable = false, foreignKey = @ForeignKey(name="FK_codigo_producto"))
	private Producto producto;

}
