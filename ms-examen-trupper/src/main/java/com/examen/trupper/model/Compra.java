package com.examen.trupper.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="compra")
public class Compra {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCompra;
	
	@Column(name="nombre", length = 50)
	private String nombre;
	
	@Column(name="fecha_registro")
	private LocalDateTime fechaRegistro;
	
	@Column(name="fecha_ultima_actualizacion")
	private LocalDateTime fechaUltimaActualizacion;
	
	@Column(name="activo")
	private Integer activo;
	
	@ManyToOne
	@JoinColumn(name="id_cliente", nullable = false, foreignKey = @ForeignKey(name="FK_customer_id"))
	private Cliente cliente;
	
}
